<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_address', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room');
            $table->integer('floor');
            
            $table->timestamps();

            $table->integer('id_event');
            $table->integer('id_address');

            $table->foreign('id_event')->references('id')->on('events');
            $table->foreign('id_address')->references('id')->on('addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_address');
    }
}
