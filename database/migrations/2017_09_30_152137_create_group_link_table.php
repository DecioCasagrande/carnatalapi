<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_link', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('id_group');
            $table->integer('id_link');

            $table->foreign('id_group')->references('id')->on('groups');
            $table->foreign('id_link')->references('id')->on('links');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_link');
    }
}
