<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCupomImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupom_image', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('current_changes');
            $table->timestamps();

            $table->integer('id_cupom');
            $table->integer('id_image');

            $table->foreign('id_cupom')->references('id')->on('cupons');
            $table->foreign('id_image')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupom_image');
    }
}
