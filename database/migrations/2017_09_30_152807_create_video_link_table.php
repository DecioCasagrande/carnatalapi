<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_link', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('current_changes');
            $table->timestamps();

            $table->integer('id_video');
            $table->integer('id_link');

            $table->foreign('id_video')->references('id')->on('videos');
            $table->foreign('id_link')->references('id')->on('links');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_link');
    }
}
