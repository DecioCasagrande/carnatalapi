<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStablishmentSegmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stablishment_segment', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('id_stablishment');
            $table->integer('id_segment');

            $table->foreign('id_stablishment')->references('id')->on('stablishments');
            $table->foreign('id_segment')->references('id')->on('segments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stablishment_segment');
    }
}
