<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStablishmentCupomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stablishment_cupom', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantity');
            $table->date('start_date');
            $table->date('end_date');
            $table->boolean('status');
            $table->timestamps();

            $table->integer('id_stablishment');
            $table->integer('id_cupom');

            $table->foreign('id_stablishment')->references('id')->on('stablishments');
            $table->foreign('id_cupom')->references('id')->on('cupons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stablishment_cupom');
    }
}
