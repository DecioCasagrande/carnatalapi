<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_event', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('current_changes');
            $table->timestamps();

            $table->integer('id_product');
            $table->integer('id_event');

            $table->foreign('id_product')->references('id')->on('products');
            $table->foreign('id_event')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_event');
    }
}
