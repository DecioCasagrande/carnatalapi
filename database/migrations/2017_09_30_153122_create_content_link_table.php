<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_link', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('current_changes');
            $table->timestamps();

            $table->integer('id_content');
            $table->integer('id_link');

            $table->foreign('id_content')->references('id')->on('contents');
            $table->foreign('id_link')->references('id')->on('links');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_link');
    }
}
