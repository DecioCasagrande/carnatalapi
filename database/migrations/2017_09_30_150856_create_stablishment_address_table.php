<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStablishmentAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stablishment_address', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room');
            $table->integer('floor');
            $table->timestamps();

            $table->integer('id_stablishment');
            $table->integer('id_address');

            $table->foreign('id_stablishment')->references('id')->on('stablishments');
            $table->foreign('id_address')->references('id')->on('addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stablishment_address');
    }
}
