<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_option', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status');
            $table->timestamps();

            $table->integer('id_user');
            $table->integer('id_option');

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_option')->references('id')->on('options');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_option');
    }
}
