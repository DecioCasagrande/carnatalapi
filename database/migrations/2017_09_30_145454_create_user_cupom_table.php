<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCupomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_cupom', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status');
            $table->timestamps();

            $table->integer('id_user');
            $table->integer('id_cupom');

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_cupom')->references('id')->on('cupons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_cupom');
    }
}
