<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_option', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status');
            $table->timestamps();

            $table->integer('id_group');
            $table->integer('id_option');

            $table->foreign('id_group')->references('id')->on('groups');
            $table->foreign('id_option')->references('id')->on('options');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_option');
    }
}
