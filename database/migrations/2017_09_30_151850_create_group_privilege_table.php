<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupPrivilegeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_privilege', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user');
            $table->timestamps();

            $table->integer('id_group');
            $table->integer('id_privilege');

            $table->foreign('id_group')->references('id')->on('groups');
            $table->foreign('id_privilege')->references('id')->on('privileges');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_privilege');
    }
}
