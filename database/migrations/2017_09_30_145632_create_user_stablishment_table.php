<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStablishmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_stablishment', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('id_user');
            $table->integer('id_stablishment');

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_stablishment')->references('id')->on('stablishments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_stablishment');
    }
}
