<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_link', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('current_changes');
            $table->timestamps();
            
            $table->integer('id_image');
            $table->integer('id_link');

            $table->foreign('id_image')->references('id')->on('images');
            $table->foreign('id_link')->references('id')->on('links');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_link');
    }
}
