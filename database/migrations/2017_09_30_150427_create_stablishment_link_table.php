<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStablishmentLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stablishment_link', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('id_stablishment');
            $table->integer('id_link');

            $table->foreign('id_stablishment')->references('id')->on('stablishments');
            $table->foreign('id_link')->references('id')->on('links');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stablishment_link');
    }
}
