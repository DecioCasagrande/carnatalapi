<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Stablishment extends Model
{
    use Notifiable;
    
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name', 'full_name', 'description', 'phone', 'contact'
        ];


        public function users() {
            return $this->belongsToMany('App/User', 'user_stablishment');
        }

        public function images() {
            return $this->belongsToMany('App/Image', 'stablishment_image')
                        ->withPivot('changes');
        }

        public function links() {
            return $this->belongsToMany('App/Link', 'stablishment_link');
        }

        public function segments() {
            return $this->belongsToMany('App/Segment', 'stablishment_segment');
        }

        public function addresses(){
            return $this->belongsToMany('App/Address', 'stablishment_address')
                        ->withPivot('room', 'floor');

        }

        public function cupons() {
            return $this->belongsToMany('App/Cupom', 'stablishment_cupom')
                        ->withPivot('quantitiy', 'start_date', 'end_date', 'status');
        }
    
}
