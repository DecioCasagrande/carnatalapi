<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login', 'email', 'password', 'surname', 'phone', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function privileges() {
        return $this->belongsToMany('App/Privilege', 'user_privilege' );
    }

    public function cupons() {
        return $this->belongsToMany('App/Cupom', 'user_cupom')
                    ->withPivot('status');
    }

    public function stablishments() {
        return $this->belongsToMany('App/Stablishment', 'user_stablishment');
    }

    public function addresses() {
        return $this->belongsToMany('App/Address', 'user_address')
                    ->withPivot('room', 'floor');
    }

    public function tempAddresses() {
        return $this->belongsToMany('App/Address', 'user_temp_address')
                    ->withPivot('room', 'floor');
    }

    public function options() {
        return $this->belongsToMany('App/Option', 'user_option')
                    ->withPivot('status');
    }

}
